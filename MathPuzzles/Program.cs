﻿/*
 * This source code is available under *either* the terms of the modified BSD license *or* 
 * the MIT License (2008). See http://opensource.org/licenses/alphabetical for full text.
 *
 * Copyright (c) 2013, Lars Fagerbakke (lars@fagerbakke.net).
 * 
 * 
 * 
 * THE PROBLEM:
 * 
 * This program output every solution to creating a simple mathematical equation. The rules
 * are that you can only use the numbers between 1-9, and only once. A valid solutions is:
 * 
 *   124
 * + 659
 * = 783
 * 
 * Where a invalid solution is 
 * 
 *   123
 * + 553
 * = 676
 * 
 * Because the same number is used multiple times.
 * 
 * This program counts the total number of solutions.
 * 
 */

using System;
using System.Threading;

namespace SimpleMathEquation
{
    class Program
    {
        static void Main(string[] args)
        {
            // some variables to keep track of statistics
            int tries = 0, solutions = 0;

            // By only using the numbers between 1-9, the smallest number is 123 and the largest number is 987
            for (int a = 123; a < 987; a++)
            {
                for (int b = 123; b < 987; b++)
                {
                    int sum = a + b;                    

                    string strA = a.ToString();
                    string strB = b.ToString();
                    string strSum = sum.ToString();

                    bool test = false;

                    // test if the second line contains a digit from the first line
                    foreach (char c in strB)
                        if (c == strA[0] || c == strA[1] || c == strA[2])
                            test = true;

                    // test if the last line contains a digit from the first line
                    foreach (char c in strSum)
                        if (c == strA[0] || c == strA[1] || c == strA[2])
                            test = true;

                    // test if the last line contains a digit from the second line
                    foreach (char c in strSum)
                        if (c == strB[0] || c == strB[1] || c == strB[2])
                            test = true;

                    // test to eliminate the same digit in the same number
                    foreach (char c in strA)
                        if (strA.Replace(c.ToString(), "").Length != 2)
                            test = true;
                    
                    // test to eliminate the same digit in the same number
                    foreach (char c in strB)
                        if( strB.Replace(c.ToString(), "").Length != 2 )
                            test = true;

                    // test to eliminate the same digit in the same number
                    foreach (char c in strSum)
                        if (strSum.Replace(c.ToString(), "").Length != 2)
                            test = true;

                    // 0 is not allowed
                    if( strA.Contains("0") || strB.Contains("0") || strSum.Contains("0") )
                        test = true;

                    // the sum has to match
                    if (sum != (a + b))
                        test = true;

                    // limit the sum to 3 digits
                    if (sum >= 1000)
                        test = true;

                    if (!test)
                    {
                        solutions++;

                        Console.WriteLine("\nSolution ("+solutions+"):\n");
                        Console.WriteLine("\t  " + a);
                        Console.WriteLine("\t+ " + b);
                        Console.WriteLine("\t= " + sum);                        

                        Thread.Sleep(500);
                    }

                    tries++;
                    Console.WriteLine("Attempt #" + tries);
                }
            }

            Console.WriteLine("Number of solutions: " + solutions);
            Console.ReadLine();
        }
    }
}
